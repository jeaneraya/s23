let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function() {
			console.log("This pokemon takcled tartetPokemon")
			console.log("tartetPokemon's health is now reduced to targetPokemonHealth")
		},
		faint: function() {
			console.log("Pokemon fainted")
		}

}

console.log(myPokemon)


// Using Object Constructor
function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 3 *level
	this.attack = level

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled "  + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack)) 
	},
	this.faint = function() {
		console.log(this.name + " fainted");
	};
};


console.log("");
console.log("");
let trainer = {
	trainerName: "Jean",
	trainerAge: 27,
	pokemon:["charmander","bulbasaur"],
	friends: {
		friendName:"Kaysha",
		friendAge:28
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}

console.log("Trainer's Name:");
console.log(trainer.trainerName);

console.log("List of Pokemon");
console.log(trainer.pokemon);
console.log("Result of talk method");
trainer.talk();

let bulbasaur = new Pokemon("Bulbasaur",25);
let charmander = new Pokemon("Charmander",20);
let pikachu = new Pokemon("Pikachu",15);

console.log(pikachu);
console.log(bulbasaur);
console.log(charmander)

function reduceHealth(name,opponent){
	let newHealth = opponent.health - name.attack;
	if(newHealth <= 5){
		name.tackle(opponent);
		opponent.faint();
		opponent.health = newHealth;
	}else{
		name.tackle(opponent);
		opponent.health = newHealth;
}
}
bulbasaur.tackle(pikachu);

console.log("");
reduceHealth(charmander,pikachu);
console.log(pikachu);
reduceHealth(charmander,pikachu);
console.log(pikachu);

